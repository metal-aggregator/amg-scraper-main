package com.ryan.amg.scraper.main.handler.aws;

import com.ryan.amg.scraper.main.domain.ReviewUrls;
import com.ryan.amg.scraper.main.dto.ScrapeMainPageDTO;
import org.springframework.cloud.function.adapter.aws.SpringBootRequestHandler;

public class MainPageScraperHandler extends SpringBootRequestHandler<ScrapeMainPageDTO, ReviewUrls> {
}
