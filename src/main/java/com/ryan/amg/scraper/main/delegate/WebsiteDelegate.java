package com.ryan.amg.scraper.main.delegate;

import com.ryan.amg.scraper.main.delegate.remote.DocumentFetcher;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class WebsiteDelegate {

    private final DocumentFetcher documentFetcher;

    public WebsiteDelegate(DocumentFetcher documentFetcher) {
        this.documentFetcher = documentFetcher;
    }

    public Document retrieveWebsite(String url) throws IOException {
        return documentFetcher.fetchDocument(url);
    }

}
