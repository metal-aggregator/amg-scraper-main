package com.ryan.amg.scraper.main.dto;

import lombok.Data;

@Data
public class ScrapeMainPageDTO {
    Integer urlLimit = 0;
}
