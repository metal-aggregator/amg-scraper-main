package com.ryan.amg.scraper.main.service.amg;

import com.ryan.amg.scraper.main.delegate.WebsiteDelegate;
import lombok.AllArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
@AllArgsConstructor
public class MainPageParsingService {

    private static final Logger LOG = LoggerFactory.getLogger(MainPageParsingService.class);

    private static final String AMG_URL = "https://www.angrymetalguy.com";
    private static final String REVIEW_TARGET_CLASS = "entry-title";
    private static final String REVIEW_URL_TAG = "a";
    private static final String REVIEW_URL_ATTRIBUTE = "href";

    private final WebsiteDelegate websiteDelegate;

    public @NonNull List<String> findReviewUrls() throws IOException {
        Document amgDocument = websiteDelegate.retrieveWebsite(AMG_URL);
        LOG.debug("Raw AMG main page={}", amgDocument.toString());

        List<String> reviewUrls = new ArrayList<>();
        Elements elements = amgDocument.getElementsByClass(REVIEW_TARGET_CLASS);
        LOG.debug("Elements of class={}, elements={}", REVIEW_TARGET_CLASS, elements);
        for (Element element : elements) {
            reviewUrls.addAll(parseReviewUrlFromGroup(element));
        }
        return reviewUrls;
    }

    private List<String> parseReviewUrlFromGroup(Element groupElement) {
        List<String> reviewUrls = new ArrayList<>();
        Elements aTags = groupElement.getElementsByTag(REVIEW_URL_TAG);
        for (Element aTag : aTags) {
            String reviewTarget = aTag.attr(REVIEW_URL_ATTRIBUTE);
            reviewUrls.add(reviewTarget);
        }
        return reviewUrls;
    }

}
