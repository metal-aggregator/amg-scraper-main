package com.ryan.amg.scraper.main.service;

import com.ryan.amg.scraper.main.domain.ReviewUrls;
import com.ryan.amg.scraper.main.service.amg.MainPageParsingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
public class MainPageScrapingService {

    private final MainPageParsingService mainPageParsingService;

    public ReviewUrls retrieveReviewUrls(int urlLimit) throws IOException {
        return new ReviewUrls(mainPageParsingService.findReviewUrls().stream().limit(urlLimit).collect(Collectors.toList()));
    }

}
