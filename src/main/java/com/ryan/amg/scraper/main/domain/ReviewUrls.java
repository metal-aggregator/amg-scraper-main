package com.ryan.amg.scraper.main.domain;

import lombok.Data;

import java.util.List;

@Data
public class ReviewUrls {
    private final List<String> reviewUrls;
}
