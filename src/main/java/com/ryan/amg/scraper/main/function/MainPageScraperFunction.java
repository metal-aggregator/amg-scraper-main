package com.ryan.amg.scraper.main.function;

import com.ryan.amg.scraper.main.domain.ReviewUrls;
import com.ryan.amg.scraper.main.dto.ScrapeMainPageDTO;
import com.ryan.amg.scraper.main.service.MainPageScrapingService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.function.Function;

@Component("com.ryan.amg.scraper.main.handler.aws.MainPageScraperHandler")
@AllArgsConstructor
public class MainPageScraperFunction implements Function<ScrapeMainPageDTO, ReviewUrls> {

    private final MainPageScrapingService mainPageScrapingService;

    @Override
    public ReviewUrls apply(ScrapeMainPageDTO scrapeMainPageDTO) {
        try {
            return mainPageScrapingService.retrieveReviewUrls(scrapeMainPageDTO.getUrlLimit());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
