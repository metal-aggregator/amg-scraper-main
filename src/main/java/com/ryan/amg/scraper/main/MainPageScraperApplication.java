package com.ryan.amg.scraper.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MainPageScraperApplication {
    public static void main(String[] args) {
        SpringApplication.run(MainPageScraperApplication.class, args);
    }
}
