package com.ryan.amg.scraper.main.service

import com.ryan.amg.scraper.main.domain.ReviewUrls
import com.ryan.amg.scraper.main.service.amg.MainPageParsingService
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification
import spock.lang.Unroll

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class MainPageScrapingServiceSpec extends Specification {

    MainPageParsingService mockMainPageParsingService = Mock()

    MainPageScrapingService mainPageScrapingService = new MainPageScrapingService(mockMainPageParsingService)

    @Unroll
    def "Invoking reviewAndParseAmg correctly calls its dependencies and returns the expected list of URLs when maxUrlCount=#inputUrlLimit"() {

        given:
            List<String> mockedMainParserUrls = ['http://www.url1.com', 'http://www.url2.com']

            ReviewUrls expectedReviewUrls = new ReviewUrls(expectedReviewUrlList)

            1 * mockMainPageParsingService.findReviewUrls() >> mockedMainParserUrls

        when:
            ReviewUrls actualReviewUrls = mainPageScrapingService.retrieveReviewUrls(inputUrlLimit)

        then:
            assertReflectionEquals(expectedReviewUrls, actualReviewUrls, ReflectionComparatorMode.LENIENT_ORDER)

        where:
            expectedReviewUrlList                          | inputUrlLimit
            ['http://www.url1.com', 'http://www.url2.com'] | 3
            ['http://www.url1.com', 'http://www.url2.com'] | 2
            ['http://www.url1.com']                        | 1
            []                                             | 0

    }

}
