package com.ryan.amg.scraper.main.service.amg

import com.ryan.amg.scraper.main.delegate.WebsiteDelegate
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import org.unitils.reflectionassert.ReflectionComparatorMode
import spock.lang.Specification

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals

class MainPageParsingServiceSpec extends Specification {

    WebsiteDelegate mockWebsiteDelegate = Mock()

    MainPageParsingService mainPageParsingService = new MainPageParsingService(mockWebsiteDelegate)

    def "Invoking findReviewUrls correctly invokes the WebsiteDelegate, parses the resulting HTML doc, and returns the expected URL Strings"() {

        given:
            Document mockDocument = Mock()

            List<String> expectedUrls = ['http://www.reviewUrl1.com', 'http://www.reviewUrl2.com']

            Element mockReview1Block = Mock()
            Element mockReview1ATag = Mock()
            String mockUrl1 = 'http://www.reviewUrl1.com'

            Element mockReview2Block = Mock()
            Element mockReview2ATag = Mock()
            String mockUrl2 = 'http://www.reviewUrl2.com'

            1 * mockWebsiteDelegate.retrieveWebsite('https://www.angrymetalguy.com') >> mockDocument
            1 * mockDocument.getElementsByClass('entry-title') >> new Elements([mockReview1Block, mockReview2Block])

            1 * mockReview1Block.getElementsByTag('a') >> new Elements([mockReview1ATag])
            1 * mockReview1ATag.attr('href') >> mockUrl1

            1 * mockReview2Block.getElementsByTag('a') >> new Elements([mockReview2ATag])
            1 * mockReview2ATag.attr('href') >> mockUrl2

        when:
            List<String> actualUrls = mainPageParsingService.findReviewUrls()

        then:
            assertReflectionEquals(expectedUrls, actualUrls, ReflectionComparatorMode.LENIENT_ORDER)

    }

    def "Invoking findReviewUrls correctly invokes the WebsiteDelegate and returns no URLs when no review blocks are found"() {

        given:
            Document mockDocument = Mock()

            1 * mockWebsiteDelegate.retrieveWebsite('https://www.angrymetalguy.com') >> mockDocument
            1 * mockDocument.getElementsByClass('entry-title') >> new Elements([])

        when:
            List<String> actualUrls = mainPageParsingService.findReviewUrls()

        then:
            assertReflectionEquals([], actualUrls)

    }

    def "Invoking findReviewUrls correctly invokes the WebsiteDelegate and returns no URLs when no a-tags blocks are found"() {

        given:
            Document mockDocument = Mock()

            Element mockReview1Block = Mock()
            Element mockReview2Block = Mock()

            1 * mockWebsiteDelegate.retrieveWebsite('https://www.angrymetalguy.com') >> mockDocument
            1 * mockDocument.getElementsByClass('entry-title') >> new Elements([mockReview1Block, mockReview2Block])

            1 * mockReview1Block.getElementsByTag('a') >> new Elements([])
            1 * mockReview2Block.getElementsByTag('a') >> new Elements([])

        when:
            List<String> actualUrls = mainPageParsingService.findReviewUrls()

        then:
            assertReflectionEquals([], actualUrls, ReflectionComparatorMode.LENIENT_ORDER)

    }

}
