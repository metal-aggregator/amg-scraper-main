Sample input:
```json
{
  "urlLimit": 5
}
```

Sample output:
```json
{
  "reviewUrls": [
    "https://www.angrymetalguy.com/goden-beyond-darkness-review/",
    "https://www.angrymetalguy.com/holden-ursa-minor-review/",
    "https://www.angrymetalguy.com/head-of-the-demon-deadly-black-doom-review/",
    "https://www.angrymetalguy.com/green-carnation-leaves-of-yesteryear-review/",
    "https://www.angrymetalguy.com/psycode-persona-review/"
  ] 
}
```

Environment variables:
TBD